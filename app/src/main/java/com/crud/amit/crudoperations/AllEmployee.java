package com.crud.amit.crudoperations;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.crud.amit.crudoperations.Sqlite.DBOperationsHelper;
import com.crud.amit.crudoperations.model.Employee;

import java.util.List;

public class AllEmployee extends AppCompatActivity {

    private DBOperationsHelper operationsHelper;
    List<Employee> employees;
    private static final String EXTRA_EMP_ID = "empId";
    private static final String EXTRA_ADD_UPDATE = "add_update";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_employee);
        ListView ls = (ListView) findViewById(R.id.list);

        operationsHelper = new DBOperationsHelper(this);
        operationsHelper.open();
        employees = operationsHelper.getAllEmployees();
        operationsHelper.close();
        final ArrayAdapter<Employee> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, employees);
        ls.setAdapter(adapter);
        ls.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder alertDialog  = new AlertDialog.Builder(AllEmployee.this)
                        .setTitle("Update  Or Remove Employee detail")
                        .setMessage("Name :: "+employees.get(position).getFirstname()+"\nId :: "+employees.get(position).getEmpId())
                        .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Update Data", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent i = new Intent(AllEmployee.this,AddUpdateEmployee.class);
                                i.putExtra(EXTRA_ADD_UPDATE, "Update");
                                i.putExtra(EXTRA_EMP_ID, employees.get(position).getEmpId());
                                startActivity(i);

                            }
                        }).setNegativeButton("remove", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                operationsHelper.open();
                                operationsHelper.removeEmployee(operationsHelper.getEmployee(employees.get(position).getEmpId()));

                                Toast t = Toast.makeText(AllEmployee.this,"Employee removed successfully!",Toast.LENGTH_SHORT);
                                t.show();
                                employees = operationsHelper.getAllEmployees();
                                operationsHelper.close();
                                adapter.notifyDataSetChanged();
                            }
                        });
                alertDialog.create().show();


            }
        });

    }

}

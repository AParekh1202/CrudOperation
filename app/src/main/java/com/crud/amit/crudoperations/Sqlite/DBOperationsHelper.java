package com.crud.amit.crudoperations.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.crud.amit.crudoperations.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class DBOperationsHelper {

    private static final String TAG = "DBOperationsHelper";
    SQLiteOpenHelper dbhandler;
    SQLiteDatabase database;

    private static final String[] allColumns = {
            DBHelperClass.COLUMN_ID,
            DBHelperClass.COLUMN_FIRST_NAME,
            DBHelperClass.COLUMN_LAST_NAME,
            DBHelperClass.COLUMN_GENDER,
            DBHelperClass.COLUMN_HIRE_DATE,
            DBHelperClass.COLUMN_DEPT

    };

    public DBOperationsHelper(Context context){
        dbhandler = new DBHelperClass(context);
    }

    public void open(){
        Log.i(TAG,"Database Opened");
        database = dbhandler.getWritableDatabase();


    }
    public void close(){
        Log.i(TAG, "Database Closed");
        dbhandler.close();

    }
    public Employee addEmployee(Employee Employee){
        ContentValues values  = new ContentValues();
        values.put(DBHelperClass.COLUMN_FIRST_NAME,Employee.getFirstname());
        values.put(DBHelperClass.COLUMN_LAST_NAME,Employee.getLastname());
        values.put(DBHelperClass.COLUMN_GENDER, Employee.getGender());
        values.put(DBHelperClass.COLUMN_HIRE_DATE, Employee.getHiredate());
        values.put(DBHelperClass.COLUMN_DEPT, Employee.getDept());
        long insertid = database.insert(DBHelperClass.TABLE_EMPLOYEES,null,values);
        Employee.setEmpId(insertid);
        return Employee;

    }

    // Getting single Employee
    public Employee getEmployee(long id) {

        Cursor cursor = database.query(DBHelperClass.TABLE_EMPLOYEES,allColumns,DBHelperClass.COLUMN_ID + "=?",new String[]{String.valueOf(id)},null,null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Employee e = new Employee(Long.parseLong(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5));
        // return Employee
        return e;
    }

    public List<Employee> getAllEmployees() {

        Cursor cursor = database.query(DBHelperClass.TABLE_EMPLOYEES,allColumns,null,null,null, null, null);

        List<Employee> employees = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                Employee employee = new Employee();
                employee.setEmpId(cursor.getLong(cursor.getColumnIndex(DBHelperClass.COLUMN_ID)));
                employee.setFirstname(cursor.getString(cursor.getColumnIndex(DBHelperClass.COLUMN_FIRST_NAME)));
                employee.setLastname(cursor.getString(cursor.getColumnIndex(DBHelperClass.COLUMN_LAST_NAME)));
                employee.setGender(cursor.getString(cursor.getColumnIndex(DBHelperClass.COLUMN_GENDER)));
                employee.setHiredate(cursor.getString(cursor.getColumnIndex(DBHelperClass.COLUMN_HIRE_DATE)));
                employee.setDept(cursor.getString(cursor.getColumnIndex(DBHelperClass.COLUMN_DEPT)));
                employees.add(employee);
            }
        }
        // return All Employees
        return employees;
    }




    // Updating Employee
    public int updateEmployee(Employee employee) {

        ContentValues values = new ContentValues();
        values.put(DBHelperClass.COLUMN_FIRST_NAME, employee.getFirstname());
        values.put(DBHelperClass.COLUMN_LAST_NAME, employee.getLastname());
        values.put(DBHelperClass.COLUMN_GENDER, employee.getGender());
        values.put(DBHelperClass.COLUMN_HIRE_DATE, employee.getHiredate());
        values.put(DBHelperClass.COLUMN_DEPT, employee.getDept());

        // updating row
        return database.update(DBHelperClass.TABLE_EMPLOYEES, values,
                DBHelperClass.COLUMN_ID + "=?",new String[] { String.valueOf(employee.getEmpId())});
    }

    // Deleting Employee
    public void removeEmployee(Employee employee) {

        database.delete(DBHelperClass.TABLE_EMPLOYEES, DBHelperClass.COLUMN_ID + "=" + employee.getEmpId(), null);
    }


}

package com.crud.amit.crudoperations;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.crud.amit.crudoperations.Sqlite.DBOperationsHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button addEmployeeButton;

    private Button viewAllEmployeeButton;
    private DBOperationsHelper operationsHelper;
    private static final String EXTRA_EMP_ID = "empId";
    private static final String EXTRA_ADD_UPDATE = "add_update";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        operationsHelper = new DBOperationsHelper(this);
        addEmployeeButton = (Button) findViewById(R.id.button_add_employee);
        viewAllEmployeeButton = (Button) findViewById(R.id.button_view_employees);

        addEmployeeButton.setOnClickListener(this);

        viewAllEmployeeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add_employee:
                Intent i = new Intent(MainActivity.this,AddUpdateEmployee.class);
                i.putExtra(EXTRA_ADD_UPDATE, "Add");
                startActivity(i);
                break;

            case R.id.button_view_employees:
                Intent intent = new Intent(MainActivity.this, AllEmployee.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        operationsHelper.open();
    }

    @Override
    protected void onPause() {
        super.onPause();
        operationsHelper.close();

    }
}
